#include "ros/ros.h"
#include "geometry_msgs/Twist.h"
#include "sensor_msgs/LaserScan.h"
#include "std_msgs/String.h"

#include <cmath> 
#include <iostream>

ros::Publisher velocity_publisher;
ros::Subscriber scan_subscriber;

bool collision = false;
int lastTime;
int count;

void move(double vel,double angular_vel){
	geometry_msgs::Twist vel_msg;

	vel_msg.linear.x = vel;
	vel_msg.linear.y = 0;
	vel_msg.linear.z = 0;

	vel_msg.angular.x = 0;
	vel_msg.angular.y = 0;
	vel_msg.angular.z = angular_vel;

	velocity_publisher.publish(vel_msg);
}

void stop(){
	geometry_msgs::Twist vel_msg;

	vel_msg.linear.x = 0;
	vel_msg.linear.y = 0;
	vel_msg.linear.z = 0;

	vel_msg.angular.x = 0;
	vel_msg.angular.y = 0;
	vel_msg.angular.z = 0;

	velocity_publisher.publish(vel_msg);
}

void rotate(double angle,double angular_vel){

	if(angle < 0){
		angle = -angle;
		angular_vel = -angular_vel;
	}

	// Convert degree to radius
	double radius = angle / 180 * 3.1415926;

	geometry_msgs::Twist vel_msg;

	vel_msg.linear.x = 0;
	vel_msg.linear.y = 0;
	vel_msg.linear.z = 0;

	vel_msg.angular.x = 0;
	vel_msg.angular.y = 0;
	vel_msg.angular.z = angular_vel;

	double t0 = ros::Time::now().toSec();
	double t1 = ros::Time::now().toSec();
	do{
		velocity_publisher.publish(vel_msg);
		t1 = ros::Time::now().toSec();
	}while( (t1 - t0) * -angular_vel < radius);

	vel_msg.angular.z = 0;
	velocity_publisher.publish(vel_msg);
}

void RightCheck(){
	// Convert degree to radius
	double rate = 5;

	double curTime = ros::Time::now().toSec();

	// We set count to prevent turn around in the same place
	if(curTime - lastTime >= rate && count < 3){
		count++;
		rotate(90,-2);
		lastTime = ros::Time::now().toSec();
	}
}

void scanCallback(const sensor_msgs::LaserScan::ConstPtr& msg)
{
	bool scanCollisonResult = false;
	int size = sizeof(msg->ranges) / sizeof(msg->ranges[0]);
	for(int i=0;i<size;i++){
		if(msg->ranges[i] < 1){
			scanCollisonResult = true;
			break;
		}
	}
	collision = scanCollisonResult;
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "turtlebot_auto_slam_node");

  ros::NodeHandle node;

  ros::Rate startup(1);
  startup.sleep();

  ros::Rate loop_rate(10);

  velocity_publisher = node.advertise<geometry_msgs::Twist>("/mobile_base/commands/velocity",100);
  scan_subscriber = node.subscribe("/scan", 100, scanCallback);

  lastTime = ros::Time::now().toSec();
  count = 5; // So it won't do right check until it hits the wall for the first time

  while (ros::ok())
  {
  		// When collision occurs, the robot only turn left
  		if(!collision){
			move(0.5,0);
			RightCheck();
		}
		else{
			move(0,1);
			lastTime = ros::Time::now().toSec();
			count = 0;
		}

		// The robot periodically (and randomly) turns right. 
		

		ros::spinOnce();

		loop_rate.sleep();

		//break;
  }

  return 0;
}
