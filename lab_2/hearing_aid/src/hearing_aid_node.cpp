#include "ros/ros.h"
#include "std_msgs/String.h"

/**************************************************
 *
 * NOTE THAT WE ADDED THE HEADERS THAT YOU NEED.
 *
 **************************************************/
#include "std_srvs/SetBool.h"
#include <queue>
#include <algorithm>

// Global flag to know if power is on.
bool powerOn = false;
std::queue<std::string> chatterQueue;

void chatterCallback(const std_msgs::String::ConstPtr& msg)
{
	/*******************************************
	 *
	 * QUEUE THE MESSAGE FOR HANDLING.
	 * ONLY CAPITALIZE AND OUTPUT IF POWER ON.
	 *
	 *******************************************/
	if(powerOn)
	{
		chatterQueue.push(msg->data);
	}
}

bool powerSwitch(std_srvs::SetBool::Request& req,std_srvs::SetBool::Response& res)
{
	powerOn = req.data;
	res.success = true;
	return true;
}

/***************************************************
 *
 * ADD CALLBACK FOR HANDLING POWER ON SERVICE.
 *
 ***************************************************/

int main(int argc, char **argv)
{
	ros::init(argc, argv, "hearing_aid");

	ros::NodeHandle node;

	ros::Subscriber chatterSub = node.subscribe("chatter", 1000, chatterCallback);

	/*******************************************
	 *
	 * SETUP SERVICE TO TURN POWER ON.
	 * USE STANDARD SET_BOOL SERVICE MESSAGE.
	 *
	 *******************************************/
	ros::ServiceServer service = node.advertiseService("power_service", powerSwitch);
	

	/**********************************************
	 *
	 * SETUP PUBLISHER FOR NEW "hearing_aid" TOPIC.
	 *
	 **********************************************/

	ros::Publisher hearingAid_pub = node.advertise<std_msgs::String>("hearing_aid", 1000);

  	ros::Rate loop_rate(10);

	while (ros::ok())
	{
		/**********************************************
		 *
		 * HANDLE  QUEUE OF MESSAGES IF POWER IS ON.
		 * MAKE THEM ALL CAPS AND PUBLISH ON NEW TOPIC.
		 *
		 **********************************************/
		if(powerOn && !chatterQueue.empty()){

			std::string s = chatterQueue.front();

			chatterQueue.pop();

			transform(s.begin(), s.end(), s.begin(), toupper);
			
			std_msgs::String msg;

		    msg.data = s;

		    ROS_INFO("%s", msg.data.c_str()); 

			hearingAid_pub.publish(msg);
		}

		ros::spinOnce();

		loop_rate.sleep();
	}

	return 0;
}
