#include "ros/ros.h"
#include <iostream>
#include <algorithm>
// using namespace std;

/**************************************************
 *
 * NOTE THAT WE ADDED THE MESSAGE THAT YOU NEED.
 *
 **************************************************/
#include "std_srvs/SetBool.h"

/************************************************************
 *
 * MAKE SURE THAT THE CMAKELIST SETS UP THIS POWER NODE.
 *
 ************************************************************/

int main(int argc, char **argv)
{
	ros::init(argc, argv, "power");

	ros::NodeHandle node;

	/*******************************************
	 *
	 * SETUP CLIENT TO TURN POWER ON.
	 * USE STANDARD SET_BOOL SERVICE MESSAGE.
	 *
	 *******************************************/
	bool powerOn = false;

	ros::Rate loop_rate(10);

	ros::ServiceClient client = node.serviceClient<std_srvs::SetBool>("power_service");

	std_srvs::SetBool srv;


	while (ros::ok())
	{
		/******************************************************
		 *
		 * CHECK IF THE USER WOULD LIKE TO TURN POWER ON/OFF.
		 * USE THE USER INPUT WHEN IT COMES TO SEND A SERVICE.
		 *
		 *****************************************************/
		std::string instr;
		std::cout << "Please input ON/OFF:";
		getline(std::cin,instr);
		transform(instr.begin(), instr.end(), instr.begin(), toupper);

		if(instr.compare("ON") == 0)
		{
			powerOn = true;
			srv.request.data = powerOn;
			if (client.call(srv))
			{
				std::cout << "Successfully power on.\n";
			}
		}
		else if(instr.compare("OFF") == 0)
		{
			powerOn = false;
			srv.request.data = powerOn;
			if (client.call(srv))
			{
				std::cout << "Successfully power off.\n";
			}
		}
		else
		{
			std::cout << "Invalid instruction.\n";
		}

		ros::spinOnce();

		loop_rate.sleep();
	}

	return 0;
}
