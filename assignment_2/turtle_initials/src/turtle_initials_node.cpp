#include "ros/ros.h"
#include <string>
#include <iostream>

/********************************
 *
 * ADD ADDITIONAL HEADERS HERE.
 *
 ********************************/
 #include "geometry_msgs/Twist.h"
 #include "turtlesim/TeleportAbsolute.h"
 #include "turtlesim/SetPen.h"

/***************************
 *
 * ADD ANY CALLBACKS HERE.
 *
 ***************************/

ros::Publisher velocity_publisher;
ros::ServiceClient teleport_client;
ros::ServiceClient pen_client;


void move(double distance,double duration = 1);
void rotate(double degree,double duration = 1);
void circle(double r,double degree,double duration = 1);
void teleport(double pos_x,double pos_y,double theta = 0);
void setPen(uint r,uint g,uint b,uint width,bool on);

int main(int argc, char **argv)
{
	// Initialize the node.
	ros::init(argc, argv, "turtle_initials_node");
	ros::NodeHandle node;

	// Sleep on startup for 1 second.
	ros::Rate startup(1);
	startup.sleep();

	// Loop at 10Hz, publishing commands.
	ros::Rate rate(10);

	/***************************************
	 *
	 * SETUP TOPIC AND SERVICE CONNECTIONS.
	 *
	 ***************************************/
	velocity_publisher = node.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel",100);
	teleport_client = node.serviceClient<turtlesim::TeleportAbsolute>("/turtle1/teleport_absolute");
	pen_client = node.serviceClient<turtlesim::SetPen>("/turtle1/set_pen");

	// Print the start of the control.
	ROS_INFO("STARTING TURTLE INITIALS CONTROL.");

	while (ros::ok()) 
	{
		/************************************************
		 *
		 * SET UP STATE MACHINE FOR CONTROLING TURTLESIM.
		 *r
		 ************************************************/

		double begin_x = 1;
		double begin_y = 7;
 
		rotate (0); // To compensate for latency
		// draw 'W'
		teleport(begin_x,begin_y); 
		
		double d = 75; // The degree to draw 'W'
		double l = 4; // The length for each path

		rotate(-d);
		move(l);

		rotate(2 * d);
		move(l);

		rotate(-2 * d);
		move(l);

		rotate(2 * d);
		move(l);

		// draw dot
		teleport(begin_x + 5,begin_y - l);
		move(0.1,0.1);

		// draw 'C'
		teleport(begin_x + 8.5,begin_y - 0.2);
		rotate(150,0.5);
		circle(2,240);

		// draw dot
		teleport(begin_x + 9,begin_y - l);
		move(0.1,0.1);

		// Allow for processing incoming messages.
		ros::spinOnce();

		// Sleep to control the rate of publishing.
		rate.sleep();

		// Finish drawing and start repeating the dance
		setPen(0,0,0,0,false);
		move(0.5,0.5);
		while(true){
			rotate(360,2);
		}
		break;
	}
}

void rotate(double degree,double duration){
	// Convert degree to radius
	double radius = degree / 180 * 3.1415926;
	double vel_radius = radius / duration;

	geometry_msgs::Twist vel_msg;

	vel_msg.linear.x = 0;
	vel_msg.linear.y = 0;
	vel_msg.linear.z = 0;

	vel_msg.angular.x = 0;
	vel_msg.angular.y = 0;
	vel_msg.angular.z = vel_radius;

	double t0 = ros::Time::now().toSec();
	double t1 = ros::Time::now().toSec();
	do{
		velocity_publisher.publish(vel_msg);
		t1 = ros::Time::now().toSec();
	}while(t1 - t0 < duration);

	vel_msg.angular.z = 0;
	velocity_publisher.publish(vel_msg);
}

void move(double distance,double duration){

	double vel = distance / duration;

	geometry_msgs::Twist vel_msg;

	vel_msg.linear.x = vel;
	vel_msg.linear.y = 0;
	vel_msg.linear.z = 0;

	vel_msg.angular.x = 0;
	vel_msg.angular.y = 0;
	vel_msg.angular.z = 0;

	double t0 = ros::Time::now().toSec();
	double t1 = ros::Time::now().toSec();
	do{
		velocity_publisher.publish(vel_msg);
		t1 = ros::Time::now().toSec();
	}while(t1 - t0 < duration);

	vel_msg.linear.x = 0;
	velocity_publisher.publish(vel_msg);
}

void circle(double r,double degree,double duration){
	// Convert degree to radius
	double radius = degree / 180 * 3.1415926;
	double vel_radius = radius / duration;

	geometry_msgs::Twist vel_msg;

	vel_msg.linear.x = vel_radius * r;
	vel_msg.linear.y = 0;
	vel_msg.linear.z = 0;

	vel_msg.angular.x = 0;
	vel_msg.angular.y = 0;
	vel_msg.angular.z = vel_radius;

	double t0 = ros::Time::now().toSec();
	double t1 = ros::Time::now().toSec();
	do{
		velocity_publisher.publish(vel_msg);
		t1 = ros::Time::now().toSec();
	}while(t1 - t0 < duration);

	vel_msg.linear.x = 0;
	vel_msg.angular.z = 0;
	velocity_publisher.publish(vel_msg);
}

void teleport(double pos_x,double pos_y,double theta){
	setPen(69,86,255,1,true);

	ros::Rate rate(10);
	rate.sleep();

	turtlesim::TeleportAbsolute srv;
	srv.request.x = pos_x;
	srv.request.y = pos_y;
	srv.request.theta = theta;

	if(teleport_client.call(srv)){
		setPen(255,255,255,5,true);
	}
}

void setPen(uint r,uint g,uint b,uint width,bool on){
	turtlesim::SetPen srv;
	srv.request.r = r;
	srv.request.g = g;
	srv.request.b = b;
	srv.request.width = width;
	if(on){
		srv.request.off = 0;
	}
	else{
		srv.request.off = 1;
	}
	if(pen_client.call(srv)){
		// Successful
	}
}